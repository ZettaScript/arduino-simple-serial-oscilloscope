/*
 * Arduino simple serial oscilloscope
 * Copyright 2018 Pascal Engélibert
 * 
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * write analog value to Serial
 * analogRead value is between 0 and 1023 (10 bits)
 * 
 * First byte:
 * bits 0 1 2 3 4 : value
 * bits 5 6 : channel
 * bit 7 : 0
 * 
 * Second byte:
 * bits 0 1 2 3 4 : value (*32)
 * bits 5 6 : channel (*4)
 * bit 7 : 1
*/

boolean ch[6];
unsigned int val;

void setup() {
  Serial.begin(115200);
  ch[0] = true;
}

void loop() {
  if(Serial.available() > 0) {
    unsigned int comdata = Serial.read();
    unsigned int idch = comdata % 128;
    if(idch < 6)
      ch[idch] = comdata >= 128;
  }
  if(ch[0]) {
    val = analogRead(A0);
    Serial.write(val>>5);//     000xxxxx
    Serial.write(val%32+128);// 100xxxxx
    delayMicroseconds(80);
  }
  if(ch[1]) {
    val = analogRead(A1);
    Serial.write(val>>5);//     000xxxxx
    Serial.write(val%32+160);// 101xxxxx
    delayMicroseconds(80);
  }
  if(ch[2]) {
    val = analogRead(A2);
    Serial.write(val>>5);//     000xxxxx
    Serial.write(val%32+192);// 110xxxxx
    delayMicroseconds(80);
  }
  if(ch[3]) {
    val = analogRead(A3);
    Serial.write(val>>5);//     000xxxxx
    Serial.write(val%32+224);// 111xxxxx
    delayMicroseconds(80);
  }
  if(ch[4]) {
    val = analogRead(A4);
    Serial.write((val>>5)+32);//001xxxxx
    Serial.write(val%32+128);// 100xxxxx
    delayMicroseconds(80);
  }
  if(ch[5]) {
    val = analogRead(A5);
    Serial.write((val>>5)+32);//001xxxxx
    Serial.write(val%32+160);// 101xxxxx
    delayMicroseconds(80);
  }
}
