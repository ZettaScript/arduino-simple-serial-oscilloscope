#!/usr/bin/env python2

"""
Arduino simple serial oscilloscope
Copyright 2018 Pascal Engélibert

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import serial, pygame, os, sys
from time import sleep, time
from threading import Thread, RLock

ser = serial.Serial("/dev/ttyACM0")
ser.baudrate = 115200
lock = RLock()
channels = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
ach = [True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False]

class Hardware(Thread):
	def __init__(self):
		Thread.__init__(self)
		self.work = True
		self.pause = False
	
	def run(self):
		global ser, channels
		sread = ser.read
		iwait = ser.inWaiting
		last = 0
		while self.work:
			if iwait() >= 1:
				val = ord(sread(1))
				if val >= 128:
					with lock:
						channels[(val>>5)%4+(last>>5)%4*4].append(val%32+last%32*32)
				else:
					last = val
				while iwait() > 40:
					sread(40)
			sleep(0.00001)
			while self.pause:
				sleep(0.01)

size = (640,480)
pygame.init()
speed = 20
info = pygame.display.Info()

flag = pygame.RESIZABLE
if "-f" in sys.argv:
	flag = pygame.FULLSCREEN
	size = (info.current_w, info.current_h)
screen = pygame.display.set_mode(size, flag|pygame.RESIZABLE)

pygame.mouse.set_visible(False)
pygame.display.set_caption("Serial Oscilloscope")
font1 = pygame.font.SysFont("FreeMono", 20)
clock = pygame.time.Clock()
resize = 0
sresize = None
ydiv = size[1]/1024.0
vydiv = 500.0/size[1]
done = False
showlines = False
BLACK = (0,0,0)
WHITE = (255,255,255)
GREY = (127,127,127)
COLORS = [WHITE, (255,0,0), (0,255,0), (0,0,255), (255,255,0), (255,0,255), (0,255,255), (255,127,0), (255,0,127), (0,255,127), (127,255,0), (0,127,255), (127,0,255), (127,127,255), (127,255,127), (255,127,127)]
txt_pause = font1.render(" Paused ", True, BLACK, WHITE)

os.nice(4)
ser.write("\x80\x01\x02\x03\x04\x05\x06\x07\x06\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f")# turn on channel 0, off channels 1-15
hardware = Hardware()
hardware.start()

while not done:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			done = True
		elif event.type == pygame.VIDEORESIZE:
			resize = time()+0.2
			sresize = event.size
		elif event.type == pygame.KEYDOWN:
			if event.key == pygame.K_ESCAPE:
				done = True
			elif event.key == pygame.K_l:
				showlines = not showlines
			elif event.key == pygame.K_0 or event.key == pygame.K_KP0:
				ach[0] = not ach[0]
				ser.write(chr(128*ach[0]))
			elif event.key == pygame.K_1 or event.key == pygame.K_KP1:
				ach[1] = not ach[1]
				ser.write(chr(128*ach[1]+1))
			elif event.key == pygame.K_2 or event.key == pygame.K_KP2:
				ach[2] = not ach[2]
				ser.write(chr(128*ach[2]+2))
			elif event.key == pygame.K_3 or event.key == pygame.K_KP3:
				ach[3] = not ach[3]
				ser.write(chr(128*ach[3]+3))
			elif event.key == pygame.K_4 or event.key == pygame.K_KP4:
				ach[4] = not ach[4]
				ser.write(chr(128*ach[4]+4))
			elif event.key == pygame.K_5 or event.key == pygame.K_KP5:
				ach[5] = not ach[5]
				ser.write(chr(128*ach[5]+5))
			elif event.key == pygame.K_6 or event.key == pygame.K_KP6:
				ach[6] = not ach[6]
				ser.write(chr(128*ach[6]+6))
			elif event.key == pygame.K_7 or event.key == pygame.K_KP7:
				ach[7] = not ach[7]
				ser.write(chr(128*ach[7]+7))
			elif event.key == pygame.K_8 or event.key == pygame.K_KP8:
				ach[8] = not ach[8]
				ser.write(chr(128*ach[8]+8))
			elif event.key == pygame.K_9 or event.key == pygame.K_KP9:
				ach[9] = not ach[9]
				ser.write(chr(128*ach[9]+9))
			elif event.key == pygame.K_SPACE:
				hardware.pause = not hardware.pause
	
	if resize > 0 and time() > resize:
		size = sresize
		resize = 0
		screen = pygame.display.set_mode(size, flag)
		ydiv = size[1]/1024.0
		vydiv = 500.0/size[1]
	
	screen.fill(BLACK)
	
	mpos = pygame.mouse.get_pos()
	m24 = mpos[0] > 24
	mx = mpos[0]
	my = mpos[1]
	
	i = 0
	with lock:
		while i < 16:
			if ach[i]:
				while len(channels[i]) > size[0]:
					channels[i].pop(0)
			i += 1
		values = channels
	i = 0
	while i < 16:
		if ach[i]:
			x = 0
			ly = 0
			for v in values[i]:
				y = int(v*ydiv)
				if showlines:
					pygame.draw.line(screen, COLORS[i], (x-1,ly), (x,y))
					ly = y
				else:
					pygame.draw.line(screen, COLORS[i], (x,y+1), (x,y))
				if m24 and x == mx:
					txt_vm = font1.render(str(int(y*vydiv)/100.0), False, WHITE, BLACK)
					screen.blit(txt_vm, (24,4+i*20))
					txt_vm = font1.render(str(v), False, WHITE, BLACK)
					screen.blit(txt_vm, (64,4+i*20))
				x += 1
			pygame.draw.rect(screen, WHITE, (0, 10+i*20, 24, 4))
		pygame.draw.rect(screen, COLORS[i], (4, 4+i*20, 16, 16))
		i += 1
	
	if m24:
		txt_vm = font1.render(str(int(my*vydiv)/100.0), False, BLACK, WHITE)
		screen.blit(txt_vm, (4,size[1]-40))
	
	if hardware.pause:
		screen.blit(txt_pause, (4,size[1]-20))
	
	if m24:
		pygame.draw.line(screen, GREY, (mpos[0],0), (mpos[0],size[1]))
		pygame.draw.line(screen, GREY, (0,mpos[1]), (size[0],mpos[1]))
	else:
		pygame.draw.line(screen, GREY, (mpos[0],mpos[1]-8), (mpos[0],mpos[1]+8))
		pygame.draw.line(screen, GREY, (mpos[0]-8,mpos[1]), (mpos[0]+8,mpos[1]))
	
	pygame.display.flip()
	clock.tick(speed)

pygame.quit()
hardware.pause = False
hardware.work = False
hardware.join()
