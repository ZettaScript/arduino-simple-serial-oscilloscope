# Arduino simple serial oscilloscope

## Hardware part

Download _oscilloscope.ino_ into the Arduino board. It works well on Arduino Uno.

The inputs are from A0 to A5 (0-5V).

## Software part

You need _python2_, _python2-pygame_ and _python2-pyserial_. If you are on Windows, replace _/dev/ttyACM0_ by _COM1_ (or _COM2_, _COM3_...).

Run _oscilloscope.py_ (option -f for fullscreen).

Each line represents an input. Enable or disable inputs with keyboard (keys **from 0 to 9**, but the Arduino can handle only 6 channels for the moment). Pause with **space**. Get more numbers by moving the mouse cursor.

Accuracy is very limited by the duration of Arduino `analogRead` function (100 µs), so the more active inputs you have, the slower it is.
